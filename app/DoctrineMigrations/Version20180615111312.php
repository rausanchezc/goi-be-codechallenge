<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Generate basic database structure
 */
class Version20180615111312 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->disableChecks();
        $this->createClientTable();
        $this->createDriverTable();
        $this->createPurchaseAddressTable();
        $this->createPurchaseTable();
        $this->enableChecks();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->disableChecks();
        $this->dropClientTable();
        $this->dropDriverTable();
        $this->dropPurchaseAddressTable();
        $this->dropPurchaseTable();
        $this->enableChecks();
    }


    private function disableChecks()
    {
        $sql=<<<SQL
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
SQL;

        $this->addSql($sql);
    }


    private function enableChecks()
    {
        $sql=<<<SQL
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SQL;

        $this->addSql($sql);
    }


    private function createClientTable()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `client` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(60) NOT NULL,
  `last_name` VARCHAR(80) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `phone` VARCHAR(25) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_unique_idx` (`email` ASC))
ENGINE = InnoDB;
SQL;

        $this->addSql($sql);
    }


    private function createDriverTable()
    {
        $sql=<<<SQL
CREATE TABLE IF NOT EXISTS `driver` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(60) NOT NULL,
  `last_name` VARCHAR(80) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
SQL;

        $this->addSql($sql);

    }

    private function createPurchaseAddressTable()
    {
        $sql =<<<SQL
CREATE TABLE IF NOT EXISTS `purchase_address` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `street` VARCHAR(150) NOT NULL,
  `city` VARCHAR(80) NOT NULL,
  `zip` VARCHAR(15) NOT NULL,
  `country` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
SQL;

        $this->addSql($sql);
    }

    private function createPurchaseTable()
    {
        // TODO CHECK ON CASCADE STATEMENTS

        $sql=<<<SQL
CREATE TABLE IF NOT EXISTS `purchase` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `client_id` INT UNSIGNED NOT NULL,
  `driver_id` INT UNSIGNED NULL,
  `address_id` INT UNSIGNED NOT NULL,
  `delivery_date` DATE NOT NULL,
  `delivery_hour` TIME NOT NULL,
  `delivery_range` TINYINT(1) UNSIGNED NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `purchase_client_idx` (`client_id` ASC),
  INDEX `purchase_driver_idx` (`driver_id` ASC),
  INDEX `purchase_address_idx` (`address_id` ASC),
  CONSTRAINT `purchase_client_fk`
    FOREIGN KEY (`client_id`)
    REFERENCES `client` (`id`)
    ON DELETE CASCADE 
    ON UPDATE CASCADE ,
  CONSTRAINT `purchase_driver_fk`
    FOREIGN KEY (`driver_id`)
    REFERENCES `driver` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `purchase_address_fk`
    FOREIGN KEY (`address_id`)
    REFERENCES `purchase_address` (`id`)
    ON DELETE CASCADE 
    ON UPDATE CASCADE )
ENGINE = InnoDB;
SQL;

        $this->addSql($sql);

    }

    private function dropClientTable()
    {
        $sql=<<<SQL
DROP TABLE IF EXISTS `client`;
SQL;
        $this->addSql($sql);

    }

    private function dropDriverTable()
    {
        $sql=<<<SQL
DROP TABLE IF EXISTS `driver`;
SQL;
        $this->addSql($sql);

    }

    private function dropPurchaseAddressTable()
    {
        $sql=<<<SQL
DROP TABLE IF EXISTS `purchase_address`;
SQL;
        $this->addSql($sql);

    }

    private function dropPurchaseTable()
    {
        $sql=<<<SQL
DROP TABLE IF EXISTS `purchase`;
SQL;
        $this->addSql($sql);

    }
    
}
