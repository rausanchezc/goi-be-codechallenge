<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Populate database
 */
class Version20180617220129 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->fillClientTable();
        $this->fillDriverTable();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // Nothing to do
    }

    private function fillClientTable()
    {
        $sql =<<<SQL
        INSERT INTO client VALUES (1, "Raúl", "Sánchez", "raul.sanchez@gmail.com", "650400500", NOW(), NULL);
        INSERT INTO client VALUES (2, "Cristina", "Valbuena", "cristina.valbuena@gmail.com", "680700700", NOW(), NULL);
        INSERT INTO client VALUES (3, "Fernando", "Sánchez", "fernando.sanchez@gmail.com", "690788455", NOW(), NULL);
SQL;

        $this->addSql($sql);
    }

    private function fillDriverTable()
    {
        $sql =<<<SQL
        INSERT INTO driver VALUES (1, "Marley", "Stand",NOW(), NULL);
        INSERT INTO driver VALUES (2, "Merilyn", "Manson", NOW(), NULL);
        INSERT INTO driver VALUES (3, "Alejandro", "Jimenez", NOW(), NULL);
SQL;

        $this->addSql($sql);
    }
}
