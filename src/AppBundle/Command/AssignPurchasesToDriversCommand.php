<?php

namespace AppBundle\Command;

use AppBundle\Service\PurchaseService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AssignPurchasesToDriversCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        parent::configure();

        $this->setName('goi:purchases:assign-driver')
            ->setDescription('Assign driver to purchases to be able to be delivered');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        /** @var PurchaseService $purchaseService */
        $purchaseService = $container->get(PurchaseService::DIC_NAME);
        /** @var LoggerInterface $logger */
        $logger = $container->get("logger");

        try {
            $logger->info("Assignation process started");
            $purchaseService->scheduleDelivery();
            $logger->info("Assignation process has finished");

            return true;
        } catch (Exception $exception) {
            $logger->error($exception->getTraceAsString());

            return false;
        }
    }

}