<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * Purchase
 * @Serializer\ExclusionPolicy("NONE")
 * @ORM\Table(name="purchase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MySQLPurchaseRepository")
 */
class Purchase
{
    use TimestampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Assert\Date()
     * @ORM\Column(name="delivery_date", type="date")
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $deliveryDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_hour", type="time")
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $deliveryHour;

    /**
     * @var integer
     *
     * @Assert\Range(min = 1, max = 8)
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="delivery_range", type="integer")
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $deliveryRange;

    /**
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @ORM\OneToOne(targetEntity="PurchaseAddress", cascade={"persist"})
     *
     * @Serializer\Groups({"details"})
     */
    private $address;

    /**
     * @var Client
     * @Assert\NotNull()
     *
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Client",
     *     inversedBy="purchases"
     * )
     *
     * @Serializer\Groups({"details"})
     * @Serializer\MaxDepth(1)
     */
    private $client;

    /**
     * @var Driver
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Driver",
     *     inversedBy="purchases"
     * )
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     *
     * @Serializer\Groups({"details"})
     * @Serializer\MaxDepth(1)
     */
    private $driver;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryHour()
    {
        return $this->deliveryHour;
    }

    /**
     * @param $deliveryHour
     */
    public function setDeliveryHour($deliveryHour)
    {
        $this->deliveryHour = $deliveryHour;
    }

    /**
     * @return integer
     */
    public function getDeliveryRange()
    {
        return $this->deliveryRange;
    }

    /**
     * @param integer $deliveryRange
     */
    public function setDeliveryRange($deliveryRange)
    {
        $this->deliveryRange = $deliveryRange;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return$this->driver;
    }

    /**
     * @param Driver $driver
     */
    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return PurchaseAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param PurchaseAddress $address
     */
    public function setAddress(PurchaseAddress $address)
    {
        $this->address = $address;
    }
}