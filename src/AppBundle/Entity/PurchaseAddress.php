<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * PurchaseAddress
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="purchase_address")
 */
class PurchaseAddress
{
    use TimestampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="street", type="string", length=150, nullable=false)
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $street;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=80, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="zip", type="string", length=15, nullable=false)
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $zip;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     * @ORM\Column(name="country", type="string", length=100, nullable=false)
     *
     * @Serializer\Groups({"list", "details"})
     */
    private $country;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}