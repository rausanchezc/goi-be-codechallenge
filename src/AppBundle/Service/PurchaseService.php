<?php

namespace AppBundle\Service;

use AppBundle\Entity\Purchase;
use AppBundle\Repository\DriverRepositoryInterface;
use AppBundle\Repository\PurchaseRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class PurchaseService
{

    const DIC_NAME = 'goi.services.purchase_service';

    /** @var  PurchaseRepositoryInterface $purchaseRepository */
    protected $purchaseRepository;

    /** @var  DriverRepositoryInterface $driverRepository */
    protected $driverRepository;

    /** @var  LoggerInterface */
    protected $logger;

    /**
     * PurchaseService constructor.
     * @param PurchaseRepositoryInterface $purchaseRepository
     */
    public function __construct(
        LoggerInterface $logger,
        PurchaseRepositoryInterface $purchaseRepository,
        DriverRepositoryInterface $driverRepository
    ) {
        $this->logger = $logger;
        $this->purchaseRepository = $purchaseRepository;
        $this->driverRepository = $driverRepository;
    }

    /**
     * @param Purchase $purchase
     */
    public function savePurchase(Purchase $purchase)
    {
        $this->purchaseRepository->save($purchase);
    }

    /**
     * @param $driverId
     * @param $deliveryDate
     * @return Purchase[]
     */
    public function getPurchasesToDeliver($driverId, $deliveryDate)
    {
        return $this
            ->purchaseRepository
            ->getPurchasesByDriverIdAndDeliveryDate($driverId, $deliveryDate);
    }


    /**
     * Method used in command: goi:purchases:assign-driver
     */
    public function scheduleDelivery()
    {
        $unassignedPurchases = $this->purchaseRepository->getPurchasesWithoutDriver();
        $driverIds = $this->driverRepository->getAllDriversIds('DESC');

        if (0 === count($unassignedPurchases) || 0 === count($driverIds))
        {
            if (0 === count($unassignedPurchases)) {
                $this->logger->info('There are not purchases without driver');
            }

            if (0 === count($driverIds)) {
                $this->logger->warning('There are not driver into the system');
            }

            return;
        }

        // Get max id from driver list
        $maxDriverId = $driverIds[0];
        $assignedPurchases = [];

        /** @var Purchase $purchase */
        foreach ($unassignedPurchases as $purchase)
        {
            $randomDriverId = rand(1, $maxDriverId);
            $randomDriver = $this->driverRepository->getDriver($randomDriverId);
            $purchase->setDriver($randomDriver);
            $assignedPurchases[] = $purchase;
        }

        $this->purchaseRepository->saveBulk($assignedPurchases);

        $this->logger->info('Assignation process has finished');

    }
}