<?php

namespace AppBundle\Repository;

use AppBundle\Repository\DriverRepositoryInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class MySQLDriverRepository extends EntityRepository implements DriverRepositoryInterface
{
    const DIC_NAME = 'goi.repositories.driver_repository';

    public function getDriver($driverId)
    {
        return $this->find($driverId);
    }


    /**
     * @return integer[]
     */
    public function getAllDriversIds($order = 'ASC')
    {
        $qb = $this
            ->createQueryBuilder('d')
            ->select('d.id')
            ->orderBy('d.id', $order)
            ->getQuery();

        return array_column($qb->execute(null, Query::HYDRATE_ARRAY), 'id');
    }
}