<?php

namespace AppBundle\Repository;

use AppBundle\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class MySQLClientRepository extends EntityRepository implements ClientRepositoryInterface
{
    const DIC_NAME = 'goi.repositories.client_repository';
}