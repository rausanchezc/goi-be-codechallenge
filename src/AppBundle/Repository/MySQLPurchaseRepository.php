<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Purchase;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use AppBundle\Repository\PurchaseRepositoryInterface;

class MySQLPurchaseRepository extends EntityRepository implements PurchaseRepositoryInterface
{
    const DIC_NAME = 'goi.repositories.purchase_repository';

    /**
     * @param Purchase $purchase
     */
    public function save(Purchase $purchase)
    {
        $em = $this->getEntityManager();
        $em->persist($purchase);
        $em->flush();
    }


    public function saveBulk(array $purchases)
    {
        $em = $this->getEntityManager();
        foreach ($purchases as $purchase) {
            $em->persist($purchase);
        }
        $em->flush();
    }

    /**
     * @param integer $purchaseId
     * @return null| object
     */
    public function getPurchase($purchaseId)
    {
        return $this->find($purchaseId);
    }

    /**
     * @param int $driverId
     * @param \DateTime $deliveryDate
     * @return Purchase[]
     */
    public function getPurchasesByDriverIdAndDeliveryDate($driverId, $deliveryDate)
    {
        return $this->createQueryBuilder('p')
            ->where('p.driver = :driverId')
            ->andWhere('p.deliveryDate = :deliveryDate')
            ->setParameter('driverId', $driverId)
            ->setParameter('deliveryDate', $deliveryDate->format('Y-m-d'))
            ->getQuery()
            ->execute();

    }

    /**
     * @return Purchase[]
     */
    public function getPurchasesWithoutDriver()
    {
        return $this->createQueryBuilder('p')
            ->where('p.driver IS NULL')
            ->getQuery()
            ->execute();
    }
}