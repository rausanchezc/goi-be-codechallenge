<?php

namespace AppBundle\Repository;


interface DriverRepositoryInterface
{
    public function getDriver($driverId);

    public function getAllDriversIds($order = 'ASC');
}