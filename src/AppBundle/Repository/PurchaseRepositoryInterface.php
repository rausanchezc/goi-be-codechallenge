<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Purchase;
use Doctrine\Common\Collections\ArrayCollection;

interface PurchaseRepositoryInterface
{
    public function save(Purchase $purchase);

    public function saveBulk(array $purchases);

    public function getPurchase($purchaseId);

    public function getPurchasesByDriverIdAndDeliveryDate($driverId, $deliveryDate);

    public function getPurchasesWithoutDriver();
}