<?php

namespace AppBundle\Utils;

class DateUtils
{
    public static function validateDate($stringDate, $format = 'Y-m-d')
    {
        $dt = \DateTime::createFromFormat($format, $stringDate);
        return $dt !== false && !array_sum($dt->getLastErrors());
    }

    public static function convertStringToDateTime($stringDate, $format = 'Y-m-d')
    {
        return \DateTime::createFromFormat($format, $stringDate);
    }
}