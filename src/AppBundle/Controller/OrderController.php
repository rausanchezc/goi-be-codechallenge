<?php

namespace AppBundle\Controller;

use AppBundle\BusinessCase\DriverOrdersToDeliver;
use AppBundle\Service\PurchaseService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * @Route("/order", name="order_")
 * Class OrderController
 * @package AppBundle\Controller
 */
class OrderController extends ApiController
{
    /**
     * @Route("")
     * @Method({"GET"})
     */
    public function getOrderAction()
    {
        /** @var PurchaseService $orderService */
        $orderService = $this->get(PurchaseService::DIC_NAME);

        return new JsonResponse(['result' => true]);
    }


    /**
     * @Route("")
     * @Method({"POST"})
     */
    public function createOrderAction()
    {
        return new JsonResponse( [], JsonResponse::HTTP_CREATED);
    }
}
