<?php

namespace AppBundle\Controller;

use AppBundle\Service\PurchaseService;
use AppBundle\Utils\DateUtils;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/driver")
 * Class DriverController
 * @package AppBundle\Controller
 */
class DriverController extends ApiController
{
    /**
     * @Route("/{driverId}/purchase")
     * @Method({"GET"})
     * @param integer $driverId
     */
    public function getDailyOrdersAction($driverId, Request $request)
    {
        $deliveryDateData = $request->get('deliveryDate');

        if (!isset($deliveryDateData) || !DateUtils::validateDate($deliveryDateData, 'Y-m-d') ) {

            $errorResponse = [ 'errors' => [
                    'deliveryDate' => 'Parameter not provided or with wrong format (yyyy-mm-dd)'
                ]
            ];

            return new JsonResponse($errorResponse, JsonResponse::HTTP_BAD_REQUEST);
        }

        /** @var PurchaseService $purchaseService */
        $purchaseService = $this->get(PurchaseService::DIC_NAME);

        /** @var LoggerInterface $logger */
        $logger = $this->getLogger();

        /** @var Serializer $serializer */
        $serializer = $this->getSerializer();

        try {
            $deliverDate = DateUtils::convertStringToDateTime($deliveryDateData);
            $purchases = $purchaseService->getPurchasesToDeliver($driverId, $deliverDate);

            $jsonPurchases = $this->JSONSerialize($serializer, $purchases, ['details']);
            $response = [
                'data' => [
                    'list' => $jsonPurchases,
                    'total' => count($purchases)
                ]
            ];

            return new JsonResponse($response, JsonResponse::HTTP_OK);

        } catch (Exception $exception) {

            $errorResponse = $this->getError($exception);
            $logger->error($exception->getTraceAsString());

            return new JsonResponse($errorResponse, JsonResponse::HTTP_OK);
        }
    }

}