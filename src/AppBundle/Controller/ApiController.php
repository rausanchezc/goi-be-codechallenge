<?php

namespace AppBundle\Controller;

use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Serializer\Serializer;


class ApiController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return new JsonResponse(["OK"]);
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        return $this->get('logger');
    }

    /**
     * @param Form $form
     * @return array errorMessages
     */
    protected function extractFormErrors(Form $form)
    {
        $iterator = new \RecursiveIteratorIterator($form->getErrors(true, true));

        $errorMessages = [];
        /** @var FormError $item */
        foreach (iterator_to_array($iterator) as $item) {
            $errorMessages[] = [
                'message' => $item->getMessage(),
                'property' => $item->getCause()->getPropertyPath()
            ];
        }

        return $errorMessages;
    }

    /**
     * @param \Exception $exception
     * @return array
     */
    protected function getError(\Exception $exception)
    {
        return [
            'error' => [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage()
            ]
        ];
    }

    /**
     * @param Serializer $serializer
     * @param object $data
     * @param array $groups
     * @return mixed
     */
    protected function JSONSerialize($serializer, $data, $groups = ['list'])
    {
        $serializationContext = SerializationContext::create();
        $serializationContext->enableMaxDepthChecks();
        $serializationContext->setGroups($groups);

        return json_decode($serializer->serialize($data, 'json', $serializationContext));
    }

    /**
     * @return Serializer
     */
    protected function getSerializer()
    {
        return $this->get('jms_serializer');
    }
}