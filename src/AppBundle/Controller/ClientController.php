<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Purchase;
use AppBundle\Form\Type\PurchaseType;
use AppBundle\Service\PurchaseService;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/client")
 * Class ClientController
 * @package AppBundle\Controller
 */
class ClientController extends ApiController
{
    /**
     * @Route("/{clientId}/purchase")
     * @Method({"POST"})
     * @param integer $clientId
     */
    public function createPurchaseAction($clientId, Request $request)
    {
        $purchaseData = json_decode($request->getContent(), true);
        $purchaseData['client'] = $clientId;

        if (!isset($purchaseData)) {
            return new JsonResponse(
                ['error' => 'Bad request'],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $purchase =  new Purchase();
        $purchaseForm = $this->createForm(PurchaseType::class, $purchase);
        $purchaseForm->submit($purchaseData);

        if (!$purchaseForm->isValid()) {

            $errorMessages = $this->extractFormErrors($purchaseForm);

            return new JsonResponse(
                ['errors' => $errorMessages],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        /** @var PurchaseService $purchaseService */
        $purchaseService = $this->get(PurchaseService::DIC_NAME);

        /** @var LoggerInterface $logger */
        $logger = $this->getLogger();

        /** @var Serializer $serializer */
        $serializer = $this->getSerializer();

        try {
            $purchaseService->savePurchase($purchase);
            $purchaseId = $purchase->getId();

            $jsonResponse = $this->JSONSerialize($serializer, $purchase, ['details']);
            $logger->info("New purchased was created id = {$purchaseId}");

            return new JsonResponse($jsonResponse, JsonResponse::HTTP_CREATED);
        } catch (Exception $exception) {

            $errorResponse = $this->getError($exception);
            $logger->error($exception->getTraceAsString());

            return new JsonResponse($errorResponse, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}