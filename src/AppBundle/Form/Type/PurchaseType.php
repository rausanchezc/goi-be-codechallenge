<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', EntityType::class, [
                'class' => 'AppBundle\Entity\Client',
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.id');
                },
                'choice_label' => 'id',
                'choice_value' => 'id'
            ])
            ->add('address', PurchaseAddressType::class)
            ->add('deliveryDate', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('deliveryRange', NumberType::class)
            ->add('deliveryHour', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Purchase',
            'csrf_protection' => false
        ]);
    }

}