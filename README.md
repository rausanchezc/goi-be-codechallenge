# Code Challenge


## Setup

- Clone repository
```
  git clone git@bitbucket.org:rausanchezc/backend-codechallenge.git
```

- Go to project folder using command line and run following command:
```
 > composer install   #this process may takes few minutes
```

### Database

 - Setup database configuration going to the file `app/config/parameters.yml` (by default database name is `goi_db`)

 - Create database using command line

```
  > mysql -u<user> -p<password>

  > create database goi_db;
```

### Migrations

- Executing following command database structure will be created and tables `client` and `driver` will be fill up with some examples.
```
  > php bin/console doctrine:migrations:migrate
```


## Run and Routes

```
  > php bin/console server:run

  > php bin/console server:start (daemon mode)
```

### Routes

- Base route
```
GET /api
```

- New client purchase
```
Request:

POST /api/client/{clientId}/purchase

{
"address": {
    "street": "Calle Cala",
    "city": "Palma de Mallorca",
    "zip": "07007",
    "country": "Spain"
	},
"deliveryDate": "2018-6-28",
"deliveryHour": "12:25",
"deliveryRange":"2"
}


Response:

{
    "id": 1,
    "delivery_date": "2018-06-28T00:00:00+02:00",
    "delivery_hour": "1970-01-01T12:25:00+01:00",
    "delivery_range": 2,
    "address": {
        "id": 1,
        "street": "Calle Cala",
        "zip": "07007",
        "country": "Spain"
    },
    "client": {
        "id": 1,
        "first_name": "Raúl",
        "last_name": "Sánchez",
        "email": "raul.sanchez@gmail.com",
        "phone": "650400500",
        "purchases": []
    }
}


```


- Assign purchases to list of drivers available on the system

```
   #Assign driver to purchases to be able to be delivered

  > php bin/console goi:purchases:assign-driver
```


- Driver purchases

```
Request:

POST /api/driver/{driverId}/purchase?deliveryDate=YYYY-mm-dd


Response:

{
    "data": {
        "list": [
            {
                "id": 1,
                "delivery_date": "2018-06-28T00:00:00+02:00",
                "delivery_hour": "1970-01-01T12:25:00+01:00",
                "delivery_range": 2,
                "address": {
                    "id": 1,
                    "street": "Calle Cala",
                    "zip": "07007",
                    "country": "Spain"
                },
                "client": {
                    "id": 1,
                    "first_name": "Raúl",
                    "last_name": "Sánchez",
                    "email": "raul.sanchez@gmail.com",
                    "phone": "650400500",
                    "purchases": []
                },
                "driver": {
                    "id": 1,
                    "first_name": "Marley",
                    "last_name": "Stand",
                    "purchases": []
                }
            }
        ],
        "total": 1
    }
}

```
